import os
import json
import time
from copy import deepcopy
from string import Template
import random
import signal
from subprocess import Popen

import tango
from dsconfig.utils import get_devices_from_dict
from dsconfig.configure import configure
from dsconfig.dump import get_db_data
from dsconfig.filtering import filter_config
from dsconfig.formatting import SERVERS_LEVELS

# A random string to be used for uniquifying names
PREFIX = "".join(random.sample("ABCDEFGHIJKLMNOPQRTTUVWXYZ1234567890", 10))


def prefix(name):
    return PREFIX + "-" + name


def add_prefix(config, prefix):
    # Add the given prefix to all instance and device names
    modified = deepcopy(config)  # cheating!
    srvs = modified["servers"]
    for srvname, srv in list(srvs.items()):
        for instname, inst in list(srv.items()):
            srvs[srvname][prefix + "-" + instname] = srv.pop(instname)
            for clsname, clss in list(inst.items()):
                for devname in list(clss.keys()):
                    clss[prefix + "-" + devname] = clss.pop(devname)
    return modified


class Server:

    """This pytest fixture provides a Tango server with whatever devices
    inside. Note that if no path is given, the executable is assumed to
    reside in the $PATH.
    """

    def __init__(self, request, name, config, exe=None, path="", retries=50):

        self.name = name
        self.process = None
        self.config = {}
        self.proxies = {}

        # the finalizer is run after all tests are done
        request.addfinalizer(self.teardown)

        # substitute any template variables in the file, e.g. $PREFIX
        config = json.loads(
            Template(json.dumps(config)).substitute(PREFIX=PREFIX),
        )

        # get the part of the config that concerns us, ignore the rest
        srv, inst = name.split("/")

        filtered = {
            "servers": filter_config(
                filter_config(config["servers"], ["server:%s" % srv], SERVERS_LEVELS),
                ["instance:%s" % inst],
                SERVERS_LEVELS,
            )
        }

        if not filtered["servers"]:
            raise ValueError("Failed to find configuration for server %s!" % name)

        # By adding a random prefix to servers and devices in the
        # config, we can be reasonably sure to not collide with existing stuff.
        self.config = add_prefix(filtered, PREFIX)
        self.prefixed_name = srv + "/" + PREFIX + "-" + inst

        update_tango_db_with_config(self.config)

        # Run our device in the background
        # (Note: There should really only ever be one server...)
        self.process = Popen(
            [os.path.join(path, exe or srv), PREFIX + "-" + inst, "-v3"]
        )

        # create proxies for device
        for _, _, _, dev in get_devices_from_dict(self.config["servers"]):
            proxy = tango.DeviceProxy(dev)
            for i in range(retries):  # Let's wait for it to come alive
                try:
                    proxy.ping()
                    break
                except tango.DevFailed:
                    time.sleep(0.2)
            else:
                raise (RuntimeError("Device %s did not start up!" % dev))
            self.proxies[dev.upper()] = proxy

    def pause(self):
        if self.process:
            self.process.send_signal(signal.SIGSTOP)

    def resume(self):
        if self.process:
            self.process.send_signal(signal.SIGCONT)

    def teardown(self):

        # stop the server
        if self.process:
            self.process.terminate()
            self.process.wait()

        # finally, clean up in the database
        tango.Database().delete_server(self.prefixed_name)


class Device(object):

    """This fixture provides a single device running in its
    own server process. The device can be accessed through the
    'proxy' attribute."""

    def __init__(self, request, device, config, started_state, path="", retries=50):

        self.processes = {}
        self.config = {}

        # the finalizer is run after all tests are done
        request.addfinalizer(self.teardown)

        # substitute any template variables in the file, e.g. $PREFIX
        config = json.loads(
            Template(json.dumps(config)).substitute(PREFIX=PREFIX),
        )

        # config = json.loads(tmp, object_hook=decode_dict)

        # get the part of the config that concerns us, ignore the rest
        filtered = {
            "servers": filter_config(
                config["servers"], ["device:%s" % device], SERVERS_LEVELS
            )
        }

        if not filtered["servers"]:
            raise ValueError("Failed to find configuration for device %s!" % device)

        # By adding a random prefix to servers and devices in the
        # config, we can be reasonably sure to not collide with existing stuff.
        self.config = add_prefix(filtered, PREFIX)

        update_tango_db_with_config(self.config)

        # Run our device in the background
        # (Note: There should really only ever be one server...)
        for srvname, insts in self.config["servers"].items():
            for instname in insts:
                self.processes[instname] = Popen(
                    [os.path.join(path, srvname), instname, "-v3"]
                )

        # create proxy for device
        self.proxy = tango.DeviceProxy(PREFIX + "-" + device)
        for i in range(retries):  # Let's wait for it to come alive
            try:
                if self.proxy.State() == started_state:
                    break
            except (AttributeError, tango.DevFailed):
                pass
            time.sleep(0.2)
        else:
            raise (RuntimeError("Device %s did not start up!" % device))
        time.sleep(0.1)

    def pause(self):
        for process in self.processes.values():
            process.send_signal(signal.SIGSTOP)

    def resume(self):
        for process in self.processes.values():
            process.send_signal(signal.SIGCONT)

    def teardown(self):

        # stop the server
        for proc in self.processes.values():
            proc.terminate()
            proc.wait()

        # finally, clean up in the database
        for servername, srv in self.config["servers"].items():
            for instname in srv:
                tango.Database().delete_server(servername + "/" + instname)


def update_tango_db_with_config(config):
    db = tango.Database()
    current_config = get_db_data(db, dservers=True)
    db_calls = configure(config, current_config, update=True)
    for method, args, kwargs in db_calls:
        getattr(db, method)(*args, **kwargs)
