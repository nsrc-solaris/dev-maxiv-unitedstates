#!/usr/bin/env python

from tango.server import Device, command
from tango import DevState


class UnitedStatesTestDummy(Device):
    def init_device(self):
        self.set_state(DevState.STANDBY)

    @command()
    def On(self):
        self.set_state(DevState.ON)

    @command()
    def Off(self):
        self.set_state(DevState.OFF)


if __name__ == "__main__":
    UnitedStatesTestDummy.run_server()
