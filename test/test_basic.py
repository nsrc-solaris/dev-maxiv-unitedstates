"""
System tests for UnitedStates device.

This test will start a number of "dummy" devices and UnitedStates
devices configured to act as a system. The servers and devices will
have "uniquified" names with a random string prefix to prevent name
collisions. Unless something goes horribly wrong, they will also be
removed at the end of the tests.

Requirements:

* a *non-production* Tango database (stuff will be written to it)
* the "dsconfig" python module for configuring the DB
* the unitedstates module in your $PYTHONPATH and the UnitedStates
  start script in your $PATH.

"""

import json
import os
import random
import time

import tango
import pytest

from unitedstates.limitedsizedict import LimitedSizeDict
from unitedstates.ttldict import TTLDict

from fixture import Server, Device, prefix


# # #  CONFIGURATION  # # #

POLL_PERIOD = 0.1
SLIGHTLY_MORE_THAN_POLL_PERIOD = 1.1 * POLL_PERIOD
DELAY_FROM_ON_UNTIL_PUBLISHING_FIRST_UPDATE = 1.0  # see UnitedStates.start()

CONFIG = {
    "servers": {
        "UnitedStatesTestDummy": {
            "1": {
                "UnitedStatesTestDummy": {
                    # a bunch of dummy devices with polling on State
                    # (necessary for the correct operation of the US device)
                    "test/dummy/0": {
                        "properties": {"polled_attr": ["State", POLL_PERIOD * 1000]}
                    },
                    "test/dummy/1": {
                        "properties": {"polled_attr": ["State", POLL_PERIOD * 1000]}
                    },
                    "test/dummy/2": {
                        "properties": {"polled_attr": ["State", POLL_PERIOD * 1000]}
                    },
                    "test/dummy/3": {
                        "properties": {"polled_attr": ["State", POLL_PERIOD * 1000]}
                    },
                    "test/dummy/4": {
                        "properties": {"polled_attr": ["State", POLL_PERIOD * 1000]}
                    },
                    # A non-polled dummy
                    "test/dummy/5": {},
                }
            }
        },
        "UnitedStates": {
            "1": {
                "UnitedStates": {
                    "test/state/1": {
                        "properties": {
                            # include all dummies plus one nonexistent
                            "Devices": ["$PREFIX-test/dummy/%d" % i for i in range(7)]
                        }
                    }
                }
            }
        },
    }
}

dummy_devs = [prefix("TEST/DUMMY/%d") % i for i in range(5)]


# # #  FIXTURES # # #


@pytest.fixture()
def dummies(request):
    "A bunch of dummy devices that have changeable State"
    path = os.path.dirname(__file__)  # dummy.py is in this directory
    return Server(
        request,
        "UnitedStatesTestDummy/1",
        CONFIG,
        path=path,
        exe="UnitedStatesTestDummy.py",
    )


@pytest.fixture()
def unitedstates_still_starting(request, dummies):
    "A UnitedStates device still starting, configured to listen to the dummies"
    return Device(request, "test/state/1", CONFIG, tango.DevState.UNKNOWN)


@pytest.fixture()
def unitedstates(request, dummies):
    "A UnitedStates device in ON state, configured to listen to the dummies"
    return Device(request, "test/state/1", CONFIG, tango.DevState.ON)


# # #  Helpers  # # #


# # #  TESTS  # # #


@pytest.mark.tangodb
def test_device_starts_in_unknown(unitedstates_still_starting):
    proxy = unitedstates_still_starting.proxy
    assert proxy.State() == tango.DevState.UNKNOWN


@pytest.mark.tangodb
def test_device_is_initialising_after_1_second(unitedstates_still_starting):
    time.sleep(1.1)
    proxy = unitedstates_still_starting.proxy
    assert proxy.State() == tango.DevState.INIT


@pytest.mark.tangodb
def test_device_is_running(unitedstates):
    assert unitedstates.proxy.State() == tango.DevState.ON


@pytest.mark.tangodb
def test_device_re_init_does_not_timeout_or_raise_exception(unitedstates):
    unitedstates.proxy.Init()


@pytest.mark.tangodb
def test_device_reports_all_child_device_states(unitedstates):
    childdata = json.loads(unitedstates.proxy.read_attribute("childData").value)
    assert len(childdata["devices"]) == len(dummy_devs)
    for dev in dummy_devs:
        assert dev in childdata["devices"]


@pytest.mark.tangodb
def test_child_device_state_change_is_detected(dummies, unitedstates):
    child = _switch_on_random_dummy(dummies)
    state_rows = unitedstates.proxy.GetFilteredDevices([child, "ON"])
    _, state, _ = state_rows[0].split("\t")
    assert state == "ON"


@pytest.mark.tangodb
def test_device_filter_single_device(dummies, unitedstates):
    number = random.randint(0, 4)
    expected_dev = dummy_devs[number]
    expected_state = str(dummies.proxies[expected_dev].State())
    state_rows = unitedstates.proxy.GetFilteredDevices(["dummy/%d" % number])
    dev, state, timestamp = state_rows[0].split("\t")
    assert len(state_rows) == 1
    assert dev == expected_dev
    assert state == expected_state
    assert 0.0 < float(timestamp) < time.time()


@pytest.mark.tangodb
def test_device_filter_many_devices(unitedstates):
    result = unitedstates.proxy.GetFilteredDevices(["dummy/[012]"])
    assert len(result) == 3


@pytest.mark.tangodb
def test_device_reports_bad_child_devices(unitedstates):
    bad = unitedstates.proxy.BadDevices()
    assert len(bad) == 2  # one unpolled and one nonexistent


@pytest.mark.tangodb
def test_many_child_state_changes_detected(dummies, unitedstates):
    changes = 500
    initial_data = json.loads(unitedstates.proxy.ChildData)
    _toggle_state_on_all_dummies(changes, dummies)
    final_data = json.loads(unitedstates.proxy.ChildData)
    initial_version = initial_data["version"]
    final_version = final_data["version"]
    expected_changes = changes * len(dummy_devs)
    assert final_version - initial_version == expected_changes


@pytest.mark.tangodb
def test_device_pause_resume(unitedstates):
    proxy = unitedstates.proxy
    assert proxy.ping() > 0
    unitedstates.pause()
    with pytest.raises(tango.DevFailed):
        proxy.ping()
    unitedstates.resume()
    assert proxy.ping() > 0


@pytest.mark.tangodb
def test_child_event_errors_detected(dummies, unitedstates):
    proxy = unitedstates.proxy
    initial_data = json.loads(proxy.ChildData)

    _induce_one_polling_thread_late_error_on_each_device(dummies)

    expected_errors_per_device = 1
    expected_error_counts = dict.fromkeys(dummy_devs, expected_errors_per_device)
    error_counts = _count_errors_since_initial_state(initial_data, proxy)
    assert error_counts == expected_error_counts


def _induce_one_polling_thread_late_error_on_each_device(dummies):
    # pause dummy device server process to induce one
    # "polling thread is late" error on each dummy device
    dummies.pause()
    time.sleep(POLL_PERIOD * 2)
    dummies.resume()
    time.sleep(SLIGHTLY_MORE_THAN_POLL_PERIOD)


def _count_errors_since_initial_state(initial_data, proxy):
    final_data = json.loads(proxy.ChildData)
    initial_version = initial_data["version"]
    final_version = final_data["version"]
    error_counts = dict.fromkeys(dummy_devs, 0)
    for version in range(initial_version, final_version):
        updates_str = proxy.GetUpdates([version, version + 1])
        updates = json.loads(updates_str)
        for device, update in updates.items():
            error_counts[device] += update.get("errors", 0)
    return error_counts


@pytest.mark.tangodb
def test_startup_state_published_after_delay_via_change_event(dummies, unitedstates):

    change_events = []

    def handle_change_event(event):
        assert not event.err
        data = json.loads(event.attr_value.value)
        change_events.append(data)

    unitedstates.proxy.subscribe_event(
        "ChildData", tango.EventType.CHANGE_EVENT, handle_change_event
    )
    child = _switch_on_random_dummy(dummies)
    time.sleep(DELAY_FROM_ON_UNTIL_PUBLISHING_FIRST_UPDATE)

    expected_events = 2  # 1 from subscription + 1 published by device
    assert len(change_events) == expected_events

    state_v1 = change_events[0]
    state_v2 = change_events[1]
    assert "state" in state_v1
    assert "version" in state_v1
    assert "devices" in state_v1
    assert "last_update" in state_v1
    assert set(state_v1.keys()) == set(state_v2.keys())
    assert state_v1["devices"][child]["state"] == "STANDBY"
    assert state_v2["devices"][child]["state"] == "ON"
    assert state_v2["version"] == state_v1["version"] + 1


@pytest.mark.tangodb
def test_many_child_state_changes_published_via_data_ready_event(dummies, unitedstates):

    data_ready_events = []

    def handle_data_ready_event(event):
        assert not event.err
        assert event.ctr != -1
        data_ready_events.append(event.ctr)

    initial_data = json.loads(unitedstates.proxy.ChildData)
    unitedstates.proxy.subscribe_event(
        "ChildData", tango.EventType.DATA_READY_EVENT, handle_data_ready_event
    )

    changes = 10
    _toggle_state_on_all_dummies(changes, dummies)

    final_data = json.loads(unitedstates.proxy.ChildData)
    initial_version = initial_data["version"]
    final_version = final_data["version"]
    expected_changes = changes * len(dummy_devs)

    assert len(data_ready_events) == expected_changes
    assert data_ready_events[0] == initial_version + 1
    assert data_ready_events[-1] == final_version


def _switch_on_random_dummy(dummies):
    child = random.choice(dummy_devs)
    dummies.proxies[child].On()  # change state on child
    time.sleep(SLIGHTLY_MORE_THAN_POLL_PERIOD)
    return child


def _toggle_state_on_all_dummies(changes, dummies):
    for count in range(changes):
        for child in dummy_devs:
            if count % 2 == 0:
                dummies.proxies[child].On()
            else:
                dummies.proxies[child].Off()
        time.sleep(SLIGHTLY_MORE_THAN_POLL_PERIOD)


class TestLimitedSizeDict:
    def test_default_size_is_big(self):
        limited_dict = LimitedSizeDict()
        expected_len = 10000
        for index in range(expected_len):
            limited_dict[index] = index
        assert len(limited_dict) == expected_len

    def test_can_add_expected_number_of_items(self):
        expected_len = 2
        limited_dict = LimitedSizeDict(size_limit=expected_len)
        for index in range(expected_len):
            limited_dict[index] = index
        assert len(limited_dict) == expected_len

    def test_keeps_newest_items(self):
        expected_len = 2
        limited_dict = LimitedSizeDict(size_limit=expected_len)
        limited_dict["a"] = 10
        limited_dict["b"] = 20
        limited_dict["c"] = 30
        assert len(limited_dict) == expected_len
        assert "a" not in limited_dict
        assert "b" in limited_dict
        assert "c" in limited_dict

    def test_uses_kwargs_from_init(self):
        expected_len = 3
        limited_dict = LimitedSizeDict(a=10, b=20, c=30)
        assert len(limited_dict) == expected_len
        assert limited_dict["a"] == 10
        assert limited_dict["b"] == 20
        assert limited_dict["c"] == 30

    def test_limits_size_on_init(self):
        expected_len = 2
        limited_dict = LimitedSizeDict(size_limit=expected_len, a=10, b=20, c=30)
        assert len(limited_dict) == expected_len
        assert "a" not in limited_dict
        assert "b" in limited_dict
        assert "c" in limited_dict


class TestTTLDict:
    def test_empty_dict_has_length_zero_and_is_falsey(self):
        ttl_dict = TTLDict(default_ttl=1.0)
        assert len(ttl_dict) == 0
        assert not ttl_dict

    def test_iteration_over_items(self):
        ttl_dict = TTLDict(default_ttl=1.0)
        ttl_dict["a"] = 10
        ttl_dict["b"] = 20
        assert ttl_dict == {"a": 10, "b": 20}

    def test_length_matches_added_items(self):
        ttl_dict = TTLDict(default_ttl=1.0)
        ttl_dict["a"] = 10
        ttl_dict["b"] = 20
        assert len(ttl_dict) == 2

    def test_manual_item_removal(self):
        ttl_dict = TTLDict(default_ttl=1.0)
        ttl_dict["a"] = 10
        ttl_dict["b"] = 20
        del ttl_dict["b"]
        assert ttl_dict == {"a": 10}

    def test_new_enough_item_is_not_expired(self):
        ttl_dict = TTLDict(default_ttl=1.0)
        ttl_dict["a"] = 10
        assert not ttl_dict.is_expired("a")

    def test_item_will_be_expired_at_future_time(self):
        ttl = 1.0
        ttl_dict = TTLDict(default_ttl=ttl)
        ttl_dict["a"] = 10
        expired_time = time.time() + ttl
        assert ttl_dict.is_expired("a", now=expired_time)

    def test_too_old_item_is_expired_at_current_time(self):
        ttl = 0.001
        ttl_dict = TTLDict(default_ttl=ttl)
        ttl_dict["a"] = 10
        time.sleep(ttl)
        assert ttl_dict.is_expired("a")

    def test_is_expired_does_not_remove_expired_item_by_default(self):
        ttl = 0.001
        ttl_dict = TTLDict(default_ttl=ttl)
        ttl_dict["a"] = 10
        expired_time = time.time() + ttl
        assert ttl_dict.is_expired("a", now=expired_time)
        assert "a" in ttl_dict

    def test_is_expired_removes_expired_item_on_request(self):
        ttl = 0.001
        ttl_dict = TTLDict(default_ttl=ttl)
        ttl_dict["a"] = 10
        expired_time = time.time() + ttl
        assert ttl_dict.is_expired("a", now=expired_time, remove=True)
        assert "a" not in ttl_dict

    def test_iteration_excludes_expired_item(self):
        ttl_dict = TTLDict(default_ttl=1.0)
        ttl_dict["a"] = 10
        ttl_dict["b"] = 20
        ttl_dict.expire_at("a", timestamp=time.time())
        assert ttl_dict == {"b": 20}

    def test_length_excludes_expired_item(self):
        ttl_dict = TTLDict(default_ttl=1.0)
        ttl_dict["a"] = 10
        ttl_dict["b"] = 20
        ttl_dict.expire_at("a", timestamp=time.time())
        assert len(ttl_dict) == 1

    def test_key_error_raised_when_accessing_expired_item(self):
        ttl_dict = TTLDict(default_ttl=1.0)
        ttl_dict["a"] = 10
        ttl_dict["b"] = 20
        ttl_dict.expire_at("a", timestamp=time.time())
        with pytest.raises(KeyError):
            _ = ttl_dict["a"]
