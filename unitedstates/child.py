from copy import copy
import json
from threading import Lock, Timer
import time
import tango


def get_seconds(timeval):
    "Calculate fractional seconds from a timeval structure"
    return timeval.tv_sec + timeval.tv_usec / 1e6


class ChildDevice(object):

    """Represents a device to be monitored. This should be a
    device whose State attribute is of interest (and is polled)
    """

    def __init__(self, name, states, event_callback, logger):

        self.name = name.upper()
        self.states = states  # the global states storage
        self.event_callback = event_callback
        self.logger = logger

        self.subscription = None
        self.errors = 0

        self.proxy = tango.DeviceProxy(self.name)

    def is_ready(self):
        return True  # should we return False if the device can't be contacted?

    def make_data(self):
        return copy(self.states.get(self.name, {}))

    def push_event(self, event):
        "Take care of a normal State change event"
        self.logger.debug("%s got event %r", self.name, event)
        if event.attr_value:
            data = self.states.get(self.name, {})
            prevstate = data.get("state", "NA")
            newstate = str(event.attr_value.value)
            if newstate != prevstate or ("errors" in data):
                # we only need to send out events if there is a change of state,
                # or if there were errors to be cleared.
                t = get_seconds(event.attr_value.time)
                self.logger.info(
                    "STATE CHANGE %s: %s -> %s at %f, %d errors"
                    % (self.name, prevstate, newstate, t, data.get("errors", 0))
                )
                data = {"state": newstate, "last_update": t}
                self.event_callback(self.name, {self.name: data})
            else:
                self.logger.debug(
                    "Not sending event for update from %s; nothing changed." % self.name
                )
        elif event.err:
            self.logger.error("Got %d errors from %s!" % (len(event.errors), self.name))
            self.logger.debug("%s: %r", self.name, [str(e) for e in event.errors])
            data = self.make_data()
            orig_errors = data.get("errors", 0)
            data["errors"] = orig_errors + len(event.errors)
            self.errors += len(event.errors)
            if orig_errors < 5:
                self.event_callback(self.name, {self.name: data})
            else:
                # The plan here is to try and read the State manually
                # from broken devices, once for every 5 errors we get.
                if data["state"] != "UNKNOWN":
                    data["state"] = "UNKNOWN"
                    data["last_update"] = time.time()
                    self.event_callback(self.name, {self.name: data})
                if self.errors % 5 == 0:
                    try:
                        state = self.proxy.read_attribute("State")
                        data["state"] = str(state.value)
                        data["last_update"] = state.time.tv_sec
                        self.event_callback(self.name, {self.name: data})
                        self.errors = 0
                    except tango.DevFailed:
                        self.logger.error("Unable to read State from %s" % self.name)
        else:
            # We shouldn't get here, right?
            self.logger.warning("Got unexpected event: %r", event)

    def subscribe(self):
        "Subscribe for changes to the State attribute"
        self.subscription = self.proxy.subscribe_event(
            "State", tango.EventType.CHANGE_EVENT, self
        )
        self.logger.info("Connected to %s" % self.name)

    def cleanup(self):
        self.logger.debug("cleanup %s", self.name)
        if self.subscription:
            self.proxy.unsubscribe_event(self.subscription)


class CompoundChildDevice(object):

    """Represents a device to be monitored. This should be
    another UnitedStates device representing a number of
    other devices."""

    def __init__(self, name, states, event_callback, logger, delay=0.5):
        self.name = name.upper()
        self.states = states
        self.delay = delay
        self.event_callback = event_callback
        self.logger = logger

        self.devices = []

        self.version_update_lock = Lock()
        self.current_version = 0
        self.latest_version = 0
        self.subscriptions = []

        self.errors = 0  # keeping track of errors
        self.timer = None

        self.proxy = tango.DeviceProxy(self.name)

    def is_ready(self):
        # Check whether the device is done setting up its own listeners
        # Trying to subscribe before then seems likely
        try:
            state = self.proxy.read_attribute("State")
            return state.value == tango.DevState.ON
        except tango.DevFailed:
            return False

    def subscribe(self):
        "Subscribe for changes to the ChildData attribute."
        # Normally data ready events will be sent whenever the
        # data changes. In that case we will request the updates.
        self.subscriptions.append(
            self.proxy.subscribe_event(
                "ChildData", tango.EventType.DATA_READY_EVENT, self.handle_data_event
            )
        )

        # Also subscribe to change events that might be sent
        self.subscriptions.append(
            self.proxy.subscribe_event(
                "ChildData", tango.EventType.CHANGE_EVENT, self.handle_change_event
            )
        )

    def handle_change_event(self, event):
        "Handler for CHANGE events"
        if event.attr_value:
            try:
                data = json.loads(event.attr_value.value)
            except (TypeError, ValueError):
                self.logger.error(
                    "Could not decode event data %r", event.attr_value.value
                )
                return
            if data and "devices" in data:
                devices = data["devices"]
                self.devices = list(devices.keys())
                updates = dict(
                    (dev, d) for dev, d in devices.items() if d != self.states.get(dev)
                )
                if updates:
                    version = data.get("version", 0)
                    self.logger.debug(
                        "Got change event from %s (%d states, v %d)"
                        % (self.name, len(updates), version)
                    )
                    with self.version_update_lock:
                        self.current_version = version
                    self.event_callback(self.name, updates, full=True)
        # TODO: what about errors?

    def handle_data_event(self, event):
        "Handler for DATA_READY events"
        if event.ctr != -1:
            with self.version_update_lock:
                self.latest_version = event.ctr
            if self.delay > 0:
                # We'll throttle the updates a bit, collecting events
                # for a little while before updating our data. This is
                # more efficient if there are many updates in a short time.
                if not self.timer:
                    self.timer = Timer(self.delay, self._update)
                    self.timer.start()
            else:  # no delay, so we send immediately
                self._update()
        else:
            # This probably means that the underlying device has
            # stopped running
            self.logger.error("Error data ready event from %s", self.name)
            self.errors += 1

            # Go through all child devices and bump their error count
            updates = {}
            for dev in self.devices:
                data = copy(self.states[dev])
                errors = data["errors"] = data.get("errors", 0) + 1
                if errors == 1:  # new error state
                    updates[dev] = data
                elif errors == 5:
                    data["state"] = "UNKNOWN"
                    updates[dev] = data
            if updates:
                self.event_callback(self.name, updates)

    def _update(self):
        "Update and notify"
        start_time = time.time()
        try:
            self.logger.debug("%s:_update: start", self.name)
            with self.version_update_lock:
                v1 = self.current_version
                v2 = self.latest_version
                self.current_version = v2
            data = self.get_updates(v1, v2)
            self.logger.debug("%s:_update(%d), %d", self.name, v2, len(data))
            self.event_callback(self.name, data)
        except tango.DevFailed as e:
            self.logger.warn("Couldn't get updates from %s: %s", self.name, e)
            updates = {}
            for dev in self.devices:
                data = copy(self.states[dev])
                data["errors"] = 1
                data["state"] = "UNKNOWN"
                updates[dev] = data
            self.event_callback(self.name, updates)
        except Exception as e:
            self.logger.error("Unhandled exception in _update for %s: %s", self.name, e)
        finally:
            self.logger.debug(
                "%s:_update: done - %1.4f", self.name, time.time() - start_time
            )
            self.timer = None

    def get_data(self):
        "Read all child data from device"
        data = json.loads(self.proxy.read_attribute("ChildData").value)
        self.devices = list(data["devices"].keys())
        with self.version_update_lock:
            self.current_version = data["version"]
        return data

    def get_updates(self, v1, v2):
        "Get updates required to get from v1 to v2"
        try:
            updates = self.proxy.GetUpdates([v1, v2])
            return json.loads(updates)
        except tango.DevFailed as e:
            self.logger.warn("Could not fetch version interval [%d, %d]: %s", v1, v2, e)
            # This could be caused by several things; too much time
            # has elapsed so some of the updates have expired; the
            # device has restarted so its version counter has
            # reset... it should be rare, but in any case the solution
            # is simply to get the full data.
            return self.get_data()["devices"]

    def cleanup(self):
        for sub in self.subscriptions:
            self.proxy.unsubscribe_event(sub)
