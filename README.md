# UnitedStates

<img align="center" src="https://nox.apps.okd.maxiv.lu.se/widget?package=tangods-unitedstates"/>

This is a Tango device that provides an overview of the State of a set of devices. In a nutshell it monitors the State of a number of devices and provides this information in a JSON string readable from the "childData" attribute.

In the simplest case, a number of devices (or device wildcards) are listed in the Devices property (see below). When the UnitedStates device is started, it will set up listeners to all the configured devices and attempt to subscribe to change events on the State attribute.

The device does not operate properly unless the child devices are also sending State change events, as UnitedStates does not do any polling by itself. If they do not send out events by themselves they have to be configured with TANGO polling.

If a child device goes away for some reason (stopped, crashed...) the UnitedStates will wait until it has received 5 consecutive error events without any normal events, and then declare the device state as UNKNOWN until it comes back again.

It is also possible to construct a hierarchy of UnitedStates devices where devices higher up listen to the devices below. This can be useful if the number of devices is very large and spread out over many hosts. In that case using only one UnitedStates device could lead to an excessive number of sockets being created, and would take a long time to start up. It's also (possibly) more efficient to set up listeners to lots of devices running on the same machine. Finally, in a large installation it may just make sense to split devices up logically. To set this up, simply collect the "low level" UnitedStates devices in the Devices property of the higher level devices, in the same way as any device. They will be recognized as such and subscriptions will be set up correctly. There's probably never much point in having more than two levels of hierarchy.

Also note that the State of the UnitedStates device itself is, ironically, not representing its child states in any way. This may change.


## Configuration

The configuration is very simple, there is really only one property that is needed, namely "Devices". It is a simple list of device names. It is also possible to give wildcards with "*" in orded to dynamically load devices following a standard format. The device does not constantly monitor the control system for new devices being added or started; only devices that are running when the device starts up will be included. Init() should also find newly started devices.

* 'MinimumUpdatePeriod' can be set to limit how often the device will ask child UnitedStates devices for updates. This probably should not be touched unless you have a deep hierarchy of devices and want to make things quicker. Setting it to 0 avoids batching altogether, but this could be quite inefficient if there are frequent state changes.


## Attributes

* 'ChildData' is a string attribute containing the JSON encoded States for all monitored devices. It is one object where each key is a device name. The value object contains the keys:
  - state; a string representation of the current State, e.g. "ON"
  - last_update; UNIX timestamp of the last known State change for the device (Note: this is of course only accurate to the polling period on the device)
  - errors; may or may not be present, if not it can be assumed to be 0. An integer representing the current number of consecutive error events received. Intermittent errors are usually a sign that the polling needs to be tuned. Consecutive errors is a sign of more serious problems. Note that all errors are also kept in internal counters that can be accessed through the ErrorsByDevice command (see below). These counters are not reset as long as the US device is running.

ChildData sends out "data ready" events whenever new data has arrived. The "ctr" value of the event is the current version of the data. The most efficient way to stay updated is to listen to these events and use the 'GetUpdates' command (see below) to update the local data.


## Commands

* BadDevices is a list of the devices that could not be reached (not defined or not running) or did not allow a subscription to be set up on the State attribute. The latter usually means that polling was not enabled on the attribute.

* ErrorsByDevice returns a list of strings where each line contains a device name and the total number of error events received for that device. This is useful to track down misconfigured or slow devices, since they might be accumulating many errors over time.

* GetFilteredDevices takes a regular expression or substring as input and returns a list of strings where each string contains the name of a matching device and its current State. This is useful for quickly checking the state of a given device or set of devices.

* GetUpdates takes a list of two integers and returns JSON encoded data. The integers should be different versions (the first ("A") lower than the second ("B")) and the result will be the part of the state data that changed between these versions. It can be merged with state data of version A to turn it into version B. The point of this is that since the ChildData attribute sends current version as data ready events, if there are many changes happening it's more efficient to accumulate versions for a short while and then get a bunch of updates at once.


## Troubleshooting

### Devices configured do not turn up in the data ###

* Check that the devices are *running*. They must be contactable when the UnitedStates device is starting, or it will ignore them. It's fine to restart them later, the connection should be kept alive by TANGO.

* Check that the affected devices are *sending events* on the State attribute. If they do not do it by themselves, you will have to configure polling on the State attribute.

### Devices are listed as UNKNOWN even though they are running fine ###

* The UnitedStates device will mark a device as UNKNOWN if it receives more than one error event in a row, without a "healthy" event in between. This is intended to notify the user of issues that may prevent us from reliably reporting the State. The cause is usually that polling is not properly set up. If you have a lot of polled devices in one server, or if some devices are slow to respond, you're likely to get "slow polling" error events. Check the polling configuration and add more threads if needed.

### Getting intermittent errors/UNKNOWN on devices after they have been restarted ###

* First check above if this can be attributed to a polling issue.

* There is a bug in TANGO 8 (fixed in TANGO 9) that causes this to happen randomly. It's normally unlikely but if you have lots of devices (~100) running in the same server it's quite likely to happen. There is a backported patch for libtango 8.1.2 that fixes the issue.
